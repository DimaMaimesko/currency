<?php

namespace App\Services;
use Illuminate\Support\Facades\Redis;

class RedisService
{

    public function setRatesForBase()
    {
        return Redis::pipeline(function ($pipe) {
            for ($i = 0; $i < 10; $i++) {
                $pipe->hset('baseEUR',"key:$i", $i);
            }
        });
    }

    public function getRatesForBase()
    {
        return Redis::hgetall('baseEUR');
    }






}
