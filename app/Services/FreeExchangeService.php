<?php

namespace App\Services;

use GuzzleHttp\Client as GuzzleClient;
use App\Services\FixerService;

class FreeExchangeService
{



    public function getLatestRates($base)
    {
        $endpoint = 'latest';
        $client = new GuzzleClient();
        $response = $client->get('https://api.exchangeratesapi.io/'.$endpoint,[
             'query' => [
                            'base'       => $base,
                        ]
        ]);
        $contents = json_decode($response->getBody()->getContents());
        return $contents;
    }


    public function getRatesForPeriod($request)
    {

        $endpoint = 'history';
        $client = new GuzzleClient();
        $response = $client->get('https://api.exchangeratesapi.io/'.$endpoint,[
             'query' => [
                            'base'      => $request->get('base'),
                            'start_at'  => $request->get('dateStart'),
                            'end_at'    => $request->get('dateEnd'),
                            'symbols'   => $request->get('symbols'),
                        ]
        ]);
        $contents = json_decode($response->getBody()->getContents());
        return $contents;
    }



}
