<?php

namespace App\Services;

use GuzzleHttp\Client as GuzzleClient;

class FixerService
{

    public function getAllSymbols()
    {
        $endpoint = 'symbols';
        $access_key = config('siteparams.fixer_key');
        $client = new GuzzleClient();
        $response = $client->get('http://data.fixer.io/api/'.$endpoint,[
            'query' => [
                         'access_key' => $access_key,
                       ]
        ]);
       $contents = $response->getBody()->getContents();

        return $contents;
    }

    public function getLatestRates($base)
    {
        $endpoint = 'latest';
        $access_key = config('siteparams.fixer_key');
        $client = new GuzzleClient();
        $response = $client->get('http://data.fixer.io/api/'.$endpoint,[
             'query' => [
                            'access_key' => $access_key,
                            'base'       => $base,
                        ]
        ]);
        $contents = json_decode($response->getBody()->getContents());
        return $contents;
    }



}
