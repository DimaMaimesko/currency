<?php

namespace App\Http\Controllers;

use App\Services\RedisService;
use Illuminate\Http\Request;
use App\Services\FixerService;
use App\Services\FreeExchangeService;

class ExchangeController extends Controller
{

    private $exchangeService;
    private $redisService;


    public function __construct(FreeExchangeService $exchangeService, RedisService $redisService)
    {
        $this->middleware('auth');
        $this->exchangeService = $exchangeService;
        $this->redisService = $redisService;

    }


    public function index()
    {
//        dd($this->exchangeService->getLatestRates());
//        dd($this->exchangeService->getAllSymbols());
//        dd($this->exchangeService->getRatesForPeriod());
//        dd($this->redisService->getRatesForBase());
        return view('exchange');
    }



    public function chartRefresh(Request $request)
    {
        $base = $request->get('base');
        $symbols = $request->get('symbols');
        $result = $this->exchangeService->getRatesForPeriod($request);
        $values = array_values(json_decode(json_encode($result->rates), true));
        $data = [];
        foreach ($values as $symbol => $rate) {
            $data[] = $rate[$symbols];
        }

        $labels = array_keys(json_decode(json_encode($result->rates), true));
        function date_sort($a, $b) {
            return strtotime($a) - strtotime($b);
        }
        usort($labels, function ($a, $b) {
            return strtotime($a) - strtotime($b);
        });

        return [
            'labels' => $labels,
            'datasets' => array(
                [
                    'label' => $symbols,
                    'backgroundColor' => '#f87979',
                    'data' => $data
                ],

                )
        ];
    }


    public function getRates(Request $request)
    {
        $result = $this->exchangeService->getLatestRates($request->base);
        return json_encode($result);

    }

    public function getRatesPeriod(Request $request)
    {
        $result = $this->exchangeService->getRatesForPeriod($request);
        return json_encode($result);

    }
}
