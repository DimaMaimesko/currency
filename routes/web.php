<?php


Auth::routes();

Route::get('/', 'MainController@index')->name('main');

Route::get('/profile', 'ProfileController@index')->name('profile');

Route::get('/exchange', 'ExchangeController@index')->name('exchange');

Route::get('/chart-refresh', 'ExchangeController@chartRefresh')->name('chart.refresh');


Route::get('/get-rates', 'ExchangeController@getRates')->name('get.rates');

Route::get('/get-rates-period', 'ExchangeController@getRatesPeriod')->name('get.rates.period');
